import React from 'react';
import { View, StyleSheet } from 'react-native';
import Button from '../components/molecules/Button';
import Input from '../components/atoms/Input';

const AddTaskScreen = () => (
  <View style={styles.container}>
    <View style={styles.bottomContainer}>
      <View style={styles.bottomInputContainer}>
        <Input style={styles.inputStyles} />
      </View>
      <View style={styles.addBtnContainer}>
        <Button text="Add task" style={styles.addBtn} textstyle={styles.addBtnText} />
      </View>
    </View>

  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bottomInputContainer: {
    flex: 1,
  },
  bottomContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
  },
  addBtnContainer: {
  },
  addBtn: {
  },
  addBtnText: {},
  inputStyles: {
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,

  },
});
export default AddTaskScreen;
