import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import TextComp from '../components/atoms/TextComp';

const image = require('../images/writing.png');

export default function SplashScreen() {
  return (
    <View style={styles.container}>
      <View style={styles.centerContent}>
        <View>
          <Image source={image} />
        </View>
        <View style={styles.centerContentText}>
          <TextComp text="To-Do Application" style={styles.centerText} />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  centerContent: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  centerText: {
    fontSize: 35,
    fontFamily: 'Montserrat-Medium',
    letterSpacing: 1,
  },
  centerContentText: {
    paddingTop: 1,
  },
});
