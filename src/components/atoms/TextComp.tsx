import React from 'react';
import { View, Text, TextProps } from 'react-native';

interface text extends TextProps{
    text:string
}

export default function TextComp(props:text) {
  return (
    <View>
      <Text {...props}>{props.text}</Text>
    </View>
  );
}
