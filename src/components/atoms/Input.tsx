import React, { useState } from 'react';
import { TextInput, TextInputProps } from 'react-native';

export default function Input(props:TextInputProps) {
  const [input, setInput] = useState('');
  return (
    <TextInput value={input} onChangeText={(val) => { setInput(val); }} style={props.style} />
  );
}
