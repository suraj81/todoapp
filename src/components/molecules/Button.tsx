import React from 'react';
import { TouchableOpacity } from 'react-native';
import TextComp from '../atoms/TextComp';
import { ButtonProps } from '../../types/buttonPropsType';

export default function Button(props: ButtonProps) {
  return (
    <TouchableOpacity style={props.style}>
      <TextComp text={props.text} style={props.textstyle} />
    </TouchableOpacity>
  );
}
