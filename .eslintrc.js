module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  root: true,
  extends: [
    'plugin:react/recommended',
    'plugin:@typescript-eslint/recommended',
    'airbnb',
    'prettier',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 13,
    sourceType: 'module',
  },
  plugins: ['react', '@typescript-eslint', 'prettier'],
  rules: {
    'no-extra-semi': 2,
    'react/jsx-filename-extension': [
      2,
      {extensions: ['.js', '.jsx', '.tsx', '.ts']},
    ],
    'no-unused-vars': 2,
    'react/jsx-props-no-spreading': 0,
    'react/function-component-definition': 0,
    'no-use-before-define': 0,
    'import/extensions': 0,
    'react/destructuring-assignment': 0,
    'import/no-unresolved': 0,
    'import/no-dynamic-require': 0,
    'prettier/prettier': ['error'],
  },
};
